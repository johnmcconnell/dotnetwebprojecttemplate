$chocolateyPath = Join-Path $(Split-Path -parent $MyInvocation.MyCommand.Definition) "Chocolatey"
if(Test-Path $chocolateyPath) { remove-item -force -recurse $chocolateyPath }
New-Item $chocolateyPath -type directory
$chocInstallVariableName = "ChocolateyInstall"
#$env:ChocolateyInstall = $chocolateyPath
[Environment]::SetEnvironmentVariable($chocInstallVariableName, $chocolateyPath, [System.EnvironmentVariableTarget]::User)
iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
