IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[UsersInRoleNames]'))
DROP VIEW [UsersInRoleNames]
GO

CREATE VIEW UsersInRoleNames
as
select UserProfile.UserId as UserId, UserName, webpages_UsersInRoles.RoleId as RoleId, RoleName
from UserProfile, webpages_UsersInRoles, webpages_Roles 
where UserProfile.UserId = webpages_UsersInRoles.UserId
and webpages_UsersInRoles.RoleId = webpages_Roles.RoleId
GO